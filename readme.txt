Function to plot a stacked plot using ggplot in R

Usage: from R command line  
require(ggplot2)  
set.seed(11)
concat_vector = rlnorm(30)
df <- data.frame(a = concat_vector, b = 1:10, c = rep(LETTERS[1:3], each = 10))
stacked_plot(concat_vector = concat_vector, time_vector = 1:10, i_length = 10, i_num_categories_plotted = 3, str_filename = 'temp.pdf', str_xlabel = '', str_ylabel = '', str_title = '')


Acknowledgements: 
		From stackoverflow: http://stackoverflow.com/questions/13644529/how-to-create-a-stacked-line-plot
	               by user Sven Hohenstein

